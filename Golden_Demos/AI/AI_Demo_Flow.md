# Welcome to the GitLab Duo Demo Flow

## :dart: Introduction

The goal of this demo is to showcase an end-to-end workflow of our Duo capabilities. It is built off of the Duo workshop materials, but flows as an entire SDLC demonstration featuring Duo Plus features. 

**PLEASE NOTE:** Some of these features are in the [experimental phase](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment), which means they may be prone to outages as the dev team is actively working on enhancing them. Check out our docs [here](https://docs.gitlab.com/ee/user/ai_features.html) to see each feature's Maturity level.

## :checkered_flag: Get Started

### 1\. Open GitLab Duo Chat

Throughout this workshop, you will get AI-generated support from GitLab Duo Chat.

- [ ] To open the chat:
  1. In the lower-left corner, select the `Help` icon.
  2. Select `GitLab Duo Chat`. A drawer opens on the right side of your screen.
  3. Click on one of the predefined questions (or ask your own question)
  4. Then clear the context by typing `/reset`
  5. Finally, delete all previous conversations with the `/clean` message.

### 2\. Enable Code Suggestions

 - All of the AI features have already been [enabled at the top group level](https://docs.gitlab.com/ee/user/ai_features.html#enable-aiml-features).
    - Code Suggestions has to be enabled for at least one top-level group your account belongs to. This has already been taken care of in this environment. If you don’t have a role that lets you view the top-level group’s settings, contact a group owner when implementing this feature in your own context.
    - References:
      - [Code Suggestions on GitLab SaaS](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/saas.html)
      - [Enable Code Suggestions on self-managed GitLab](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/self_managed.html#enable-code-suggestions-on-self-managed-gitlab)

 #### Alternative Option

 Ask GitLab Duo Chat :robot: and follow the instructions

  ```plaintext
  How to enable Code Suggestions to my own account?
  ```

## :memo: Plan Our Work

### 1\. Summarize the epic

- [ ] Use Duo Chat to summarize the epic

1. Navigate to the group your project is in and select Plan -> Epics
1. Select the `Tanuki Racing Modernization` epic
1. In the lower-left corner, select the `Help` icon.
  2. Select `GitLab Duo Chat`. A drawer opens on the right side of your screen.
1. Prompt the chat with `summarize this epic`

### 2\. Summarize The Issue

- [ ] To summarize our issue

1. Click on `Create CI/CD Pipeline` issue
1. Scroll down to the Activity section of the issue and click **View Summary**
1. Show the summary of the comments in the issue.

Alternatively, summarize an issue that has other comments.

### 3\. Create the issue with AI description

We know from our project manager that our task is to secure our GitLab pipeline and fix any pressing vulnerabilities that we find. We will use GitLab's Plan capabilities to track our work.

- [ ] To create our issue:

1. Use the left hand navigation menu to click through **Plan > issues**
2. Next click **New Issue**
3. Give our issue a quick name, then click the **tanuki icon** in the description section.
4. On the dropdown click **Generate issue description**, then in the popup write "Add a delete leaderboard function to database file and delete leaderboard in app.rb"
5. Next click **submit**, then notice that the description is filled in for us.
6. We can then assign the issue to ourselves, set a due date, and click **create issue**


### 4\. Assign The Issue

- [ ] Assign issue to self

1. To assign the issue, navigate to Assignees and click `Assign yourself`

## :checkered_flag: Develop the Code

## 1\. Create a MR from the issue

- [ ] Create a new merge request from issue*

1. Scroll down to the bottom of the issue and select `Create merge request`
1. Create a new branch with name `delete-leaderboard`
1. Select a source branch or tag.
1. Select Create merge request.

* Or create MR from comment quick action /create-merge-request

## 2\. Add A New Method

- [ ] Notice that our db.rb file does not have a delete leader method, lets fix that:

1. Open merge request
1. Select `Code->Open in Web IDE``
1. Navigate to `lib/tanukiracing/db.rb`

1. Our project manager has made it clear we need this asap to get rid of fake records, so lets go ahead and use the prompt below on a new line after the **get_specific** to generate this function:

```plaintext
# write a delete leaderboard function using active record
```

## 3\. Generate Test Cases with Duo Chat

1. Open Duo Chat by selecting the Tanuki with the stars in the Web IDE
1. Highlight the `db.rb` file
1. Generate test cases by prompting `/tests db.rb`

## 4\. Use the New Method

1. We are then going to navigate to the **_lib/tanukiracing/app.rb_** file to use the new method that we wrote. On a new line within the class use the prompt below to generate our new delete route:

```plaintext
# write a post route that calls the delete_leader function from TanukiRacing::DB
```

> Please note that Code Suggestions uses your current file as reference for how to write the function, so you may need to do some slight editing for the final result

2. Now we want to commit this code to `delete-leaderboard`. Go ahead and click the **Source Control** button on the left hand side and write a commit message. Next click **Commit & Push**.
3. Next on the resulting dropdown make sure you click commit to our mr branch, then on the popup click the **Go to MR** button.

### 6\. AI In The MR

- [ ] View the MR summary of the changes

1. We have made a number of changes, so lets use the AI **View summary notes** button to add a detailed comment around all of the changes we have made.
2. To do this, locate the three dots button next to _Code_ in the top right of the merge request view. Click it, then on the resulting dropdown click the **View summary notes** option.

- [ ] View the Code Explanation
1. Navigate to **_lib/tanukiracing/app.rb_** file by going to Changes --> View file @ commit hash --> and highlight the new code added
2. Select the **question mark** and read the Code Explanation summary.

# After the merge, we can take a look at any vulnerabilties introduced

## :beetle: Analyze vulnerabilities

### 1\. Understand the security posture of your application

- [ ] See the overview of the vulnerabilities of your project
  1. On the left sidebar, select `Secure > Vulnerability report` to view the full report
  2. First, click into any of the vulnerabilities present. Notice that there are a number of vulnerabilities like token leaks & container issues, all of which GitLab will help you quickly fix through policies and the power of one platform. 
- [ ] Look for the **Possible SQL Injection** vulnerability  
  1. Filter the report as follows:
    - `Status=All statuses`
    - `Severity=Low`
    - `Tool=SAST`. 
  2. Select the vulnerability
- [ ] Get more insight about the vulnerability, how it could be exploited, and how to fix it.  
  1. At the bottom of the vulnerability’s page, click the `Try it out` button.
  2. A popup will open on the right-hand side and you will get an explanation on what a SQL injection risk is and why our application is vulnerable using [GitLab's Explain This Vulnerability](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability) functionality.
  3. Check out the **_Fixed Code_** section and we can see that if we add `sanitize_sql(id)` around our `id` value we will be protected from most attacks. We will use this knowledge later in the workshop.

> If you are curious what triggered this response try clicking **_Show prompt_** to see the full prompt sent to GitLab duo to generate the suggested fix.

### 2\. Understand the context of the vulnerability

- [ ] What if we wanted more context about the specific function above before we go and make a code change?  
  1. Let's click the linked file in the **_Location_** section to be brought to our `db.rb` file.
  2. Once in the `db.rb` file locate the line the sql injection vulnerability on line 42 and highlight the whole `get_specific` function.
  3. You should then see a small question mark to the left of the code, click it.
  4. On the right hand side there will now be a small pop up called `Code Explanation` which explains what your highlighted code does in natural language.
  5. Try highlighting other code sections as well.
  6. At this point we should be fully aware of why and how our SQL injection vulnerability is occurring. 

## :art: Implement the fix

### 1\. Resolve With AI
- [ ] Before we jump in and use Code Suggestions to help us fix our security vulnerabilities, we are also going to use the _Resolve with AI_ feature:

1. On the left sidebar, select `Secure > Vulnerability report` to view the full report again

2. Filter the report as follows:
    - `Status=All statuses`
    - `Severity=Critical`
    - `Tool=SAST`. 
3.. Click into any of the shown vulnerabilities. Once one the vulenerability view go ahead and click **Resolve with AI**.
4. This will then open up a new Merge Request that fixes our vulnerability. Click the **Changes** tab to view the fix that we are bringing in.
5. We can check back in on this later to confirm that our vulenerability is fixed, but in the mean time will move on to the next section.

# Conclusion
We saw a full lifecycle with AI embedded all throughout from planning, to coding, to securing your production application
